#include "gtest/gtest.h"
#include "lib/SqList.h"

TEST(SqListTest, InitAndDestory) {
    SqList L;
    // InitSqList
    InitSqList(L);
    EXPECT_EQ(0, L.length);
    EXPECT_EQ(10, L.maxSize);

    // DestroySqList
    DestroySqList(L);
    EXPECT_EQ(0, L.length);
    EXPECT_EQ(0, L.maxSize);
}

TEST(SqListTest, IncreaseSize) {
    SqList L;
    InitSqList(L);
    // IncreaseSqListSize
    IncreaseSqListSize(L, 10);
    EXPECT_EQ(0, L.length);
    EXPECT_EQ(20, L.maxSize);
    IncreaseSqListSize(L, 50);
    EXPECT_EQ(0, L.length);
    EXPECT_EQ(70, L.maxSize);
    DestroySqList(L);
}

TEST(SqListTest, InsertDeleteGetLocate) {
    SqList L;
    InitSqList(L);
    // InsertElem
    for (int i = 1; i <= 7; i++) {
        EXPECT_TRUE(InsertElem(L, i, i * i));
    }
    EXPECT_EQ(7, L.length);
    EXPECT_EQ(4, L.data[1]);
    EXPECT_EQ(16, L.data[3]);
    EXPECT_EQ(36, L.data[5]);
    EXPECT_EQ(49, L.data[6]);

    // GetElem
    EXPECT_EQ(1, GetElem(L, 1));
    EXPECT_EQ(9, GetElem(L, 3));
    EXPECT_EQ(25, GetElem(L, 5));
    EXPECT_EQ(36, GetElem(L, 6));

    // DeleteElem
    int e;
    EXPECT_TRUE(DeleteElem(L, 3, e));
    EXPECT_EQ(9, e);
    EXPECT_EQ(16, GetElem(L, 3));
    EXPECT_EQ(6, L.length);
    EXPECT_EQ(4, GetElem(L, 2));

    // LocateElem
    EXPECT_EQ(2, LocateElem(L, 4));
    EXPECT_EQ(1, LocateElem(L, 1));
    EXPECT_EQ(0, LocateElem(L, e));
    EXPECT_EQ(6, LocateElem(L, 49));
    DestroySqList(L);
}