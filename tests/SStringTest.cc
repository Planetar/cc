#include "gtest/gtest.h"
#include "lib/SString.h"

TEST(SStringTest, InitClearDestroy) {
    SString s;
    EXPECT_TRUE(InitSString(s));
    EXPECT_EQ(0, s.length);
    EXPECT_TRUE(ClearSString(s));
    EXPECT_EQ(0, s.length);
    EXPECT_TRUE(DestroySString(s));
    EXPECT_EQ(0, s.length);
}

TEST(SStringTest, AssignCopyCompareEmptyLength) {
    SString s1, s2;
    InitSString(s1);
    InitSString(s2);
    char src1[] = "Hello, world!";
    // assign
    EXPECT_TRUE(StrAssign(s1, src1));
    EXPECT_EQ(13, StrLength(s1));
    EXPECT_EQ(0, StrLength(s2));
    EXPECT_FALSE(StrEmpty(s1));
    EXPECT_TRUE(StrEmpty(s2));
    // copy
    EXPECT_TRUE(StrCopy(s2, s1));
    EXPECT_EQ(13, StrLength(s2));
    EXPECT_FALSE(StrEmpty(s2));
    // compare
    EXPECT_EQ(0, StrCompare(s1, s2));
    char src2[] = "hello, world!";
    EXPECT_TRUE(StrAssign(s2, src2));
    EXPECT_GT(0, StrCompare(s1, s2));
    char src3[] = "hello, wor1d!";
    EXPECT_TRUE(StrAssign(s1, src3));
    EXPECT_GT(0, StrCompare(s1, s2));
    char src4[] = "hello";
    EXPECT_TRUE(StrAssign(s2, src4));
    EXPECT_LT(0, StrCompare(s1, s2));
    DestroySString(s1);
    DestroySString(s2);
}

TEST(SStringTest, ConcatSubStringIndex) {
    SString s1, s2, target;
    InitSString(s1);
    InitSString(s2);
    char src1[] = "Hello";
    char src2[] = "World!";
    EXPECT_TRUE(StrAssign(s1, src1));
    EXPECT_TRUE(StrAssign(s2, src2));
    EXPECT_TRUE(Concat(target, s1, s2));
    EXPECT_EQ(11, StrLength(target));
    EXPECT_TRUE(SubString(s1, target, 3, 6));
    char src3[] = "lloWor";
    EXPECT_TRUE(StrAssign(s2, src3));
    EXPECT_EQ(0, StrCompare(s1, s2));
    EXPECT_FALSE(SubString(s1, target, 3, 100));
    char src4[] = "loW";
    EXPECT_TRUE(StrAssign(s1, src4));
    EXPECT_EQ(4, Index(target, s1));
    char src5[] = "koW";
    EXPECT_TRUE(StrAssign(s2, src5));
    EXPECT_EQ(0, Index(target, s2));
    DestroySString(s1);
    DestroySString(s2);
}