#include "gtest/gtest.h"
#include "lib/LinkStack.h"

TEST(LinkStackTest, InitAndDestory) {
    LinkStack s;
    InitLinkStack(s);
    EXPECT_EQ(nullptr, s);
    for (int i = 0; i < 30; i++) EXPECT_TRUE(Push(s, i));
    DestroyLinkStack(s);
    EXPECT_EQ(nullptr, s);
}

TEST(LinkStackTest, PushPopEmpty) {
    LinkStack s;
    InitLinkStack(s);
    int e;
    for (int i = 0; i < 30; i++) EXPECT_TRUE(Push(s, i));
    EXPECT_FALSE(IsEmpty(s));
    for (int i = 0; i < 30; i++) {
        EXPECT_TRUE(Pop(s, e));
        EXPECT_EQ(30 - i - 1, e);
    }
    EXPECT_FALSE(Pop(s, e));
    EXPECT_TRUE(IsEmpty(s));
    DestroyLinkStack(s);
}

TEST(LinkStackTest, GetTop) {
    LinkStack s;
    InitLinkStack(s);
    int e;
    for (int i = 0; i < 30; i++) EXPECT_TRUE(Push(s, i));
    for (int i = 0; i < 30; i++) {
        EXPECT_TRUE(GetTop(s, e));
        EXPECT_EQ(29, e);
    }
    DestroyLinkStack(s);
}