#include "gtest/gtest.h"
#include "lib/DLinkList.h"

TEST(DLinkListTest, InitEmptyDestory) {
    DLinkList L;
    // InitDLinkList
    InitDLinkList(L);
    EXPECT_EQ(nullptr, L->next);
    EXPECT_EQ(nullptr, L->prior);
    EXPECT_TRUE(DLinkListIsEmpty(L));
    EXPECT_EQ(0, Length(L));
    // DestoryDLinkList
    DestroyDLinkList(L);
}

TEST(DLinkListTest, InsertDeleteLength) {
    DLinkList L;
    InitDLinkList(L);
    EXPECT_EQ(0, Length(L));
    for (int i = 0; i < 3; i++) InsertNextNode(L, i);
    EXPECT_EQ(3, Length(L));
    for (int i = 0; i < 3; i++) InsertPriorNode(L->next, i * 10);
    EXPECT_EQ(6, Length(L));
    EXPECT_EQ(20, GetElem(L, 1)->data);
    EXPECT_EQ(10, GetElem(L, 2)->data);
    EXPECT_EQ(0, GetElem(L, 3)->data);
    EXPECT_EQ(2, GetElem(L, 4)->data);
    EXPECT_EQ(1, GetElem(L, 5)->data);
    EXPECT_EQ(0, GetElem(L, 6)->data);
    int e;
    DeleteNode(L, 3, e);
    EXPECT_EQ(0, e);
    DeleteNode(L, 3, e);
    EXPECT_EQ(2, e);
    EXPECT_EQ(1, GetElem(L, 3)->data);
    EXPECT_EQ(4, Length(L));
    EXPECT_FALSE(InsertPriorNode(L, 3));
    EXPECT_TRUE(InsertNextNode(L, 3));
    EXPECT_FALSE(DeletePriorNode(GetElem(L, 1)));
    EXPECT_TRUE(DeletePriorNode(GetElem(L, 2)));
    for (int i = 0; i < 5; i++) DeleteNextNode(L, e);
    EXPECT_EQ(0, Length(L));
    DestroyDLinkList(L);
}

TEST(DLinkListTest, GetLocateElem) {
    DLinkList L;
    InitDLinkList(L);
    for (int i = 0; i < 8; i++) InsertNextNode(L, i * i);
    EXPECT_EQ(8, Length(L));
    for (int i = 0; i < 8; i++) EXPECT_EQ(i * i, GetElem(L, 8 - i)->data);
    EXPECT_EQ(1, LocateElem(L, 1)->data);
    EXPECT_EQ(4, LocateElem(L, 4)->data);
    EXPECT_EQ(49, LocateElem(L, 49)->data);
    EXPECT_EQ(nullptr, LocateElem(L, 100));
    DestroyDLinkList(L);
}

TEST(DLinkListTest, ReverseList) {
    DLinkList L;
    InitDLinkList(L);
    for (int i = 0; i < 8; i++) InsertNextNode(L, i * i);
    EXPECT_EQ(8, Length(L));
    for (int i = 0; i < 8; i++) EXPECT_EQ(i * i, GetElem(L, 8 - i)->data);
    ReverseDLinkList(L);
    for (int i = 0; i < 8; i++) EXPECT_EQ(i * i, GetElem(L, i + 1)->data);
    DestroyDLinkList(L);
}