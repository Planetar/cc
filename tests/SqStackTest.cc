#include "gtest/gtest.h"
#include "lib/SqStack.h"

TEST(SqStackTest, InitAndDestory) {
    SqStack s;
    InitSqStack(s);
    EXPECT_EQ(-1, s.top);
    EXPECT_FALSE(s.data == nullptr);
    EXPECT_EQ(10, s.size);
    DestroySqStack(s);
    EXPECT_EQ(-1, s.top);
    EXPECT_FALSE(s.data == nullptr);
    EXPECT_EQ(0, s.size);
}

TEST(SqStackTest, PushPopEmptyFull) {
    SqStack s;
    InitSqStack(s);
    for (int i = 0; i < s.size; i++) EXPECT_TRUE(Push(s, i * i));
    EXPECT_FALSE(Push(s, 1000));
    int e;
    for (int i = 0; i < s.size; i++) {
        if (i == 0) {
            EXPECT_TRUE(IsFull(s));
            EXPECT_FALSE(IsEmpty(s));
        }
        EXPECT_TRUE(Pop(s, e));
        if (i == s.size - 1) {
            EXPECT_TRUE(IsEmpty(s));
            EXPECT_FALSE(IsFull(s));
        }
        EXPECT_EQ(((s.size - 1 - i) * (s.size - 1 - i)), e);
    }
    EXPECT_FALSE(Pop(s, e));
    DestroySqStack(s);
}

TEST(SqStackTest, GetTop) {
    SqStack s;
    InitSqStack(s);
    for (int i = 0; i < 3; i++) Push(s, i);
    int e;
    EXPECT_TRUE(GetTop(s, e));
    EXPECT_EQ(2, e);
    EXPECT_TRUE(GetTop(s, e));
    EXPECT_EQ(2, e);
    EXPECT_TRUE(Pop(s, e));
    EXPECT_TRUE(GetTop(s, e));
    EXPECT_EQ(1, e);
    EXPECT_TRUE(GetTop(s, e));
    EXPECT_EQ(1, e);
    DestroySqStack(s);
}