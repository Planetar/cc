#ifndef LIB_DLINKLIST_H_
#define LIB_DLINKLIST_H_

typedef struct DNode {
    int data;
    struct DNode *prior, *next;
} DNode, *DLinkList;

bool InitDLinkList(DLinkList &L);

bool DLinkListIsEmpty(DLinkList &L);

bool DestroyDLinkList(DLinkList &L);

bool InsertNextNode(DNode *p, int e);

bool InsertNextNode(DNode *p, DNode *s);

bool InsertPriorNode(DNode *p, int e);

bool InsertPriorNode(DNode *p, DNode *s);

bool InsertNode(DLinkList &L, int i, int e);

bool InsertNode(DLinkList &L, int i, DNode *p);

DNode *GetElem(DLinkList &L, int i);

DNode *LocateElem(DLinkList &L, int e);

bool DeletePriorNode(DNode *p, int &e);

bool DeletePriorNode(DNode *p);

bool DeleteNextNode(DNode *p, int &e);

bool DeleteNextNode(DNode *p);

bool DeleteNode(DLinkList &L, int i, int &e);

bool DeleteNode(DNode *p, int &e);

bool ReverseDLinkList(DLinkList &L);

int Length(DLinkList &L);

#endif