#include "lib/DLinkList.h"

#include <stdio.h>
#include <stdlib.h>

bool InitDLinkList(DLinkList &L) {
    L = (DNode *)malloc(sizeof(DNode));
    if (L == nullptr) return false;
    L->data = 0;
    L->prior = nullptr;
    L->next = nullptr;
    return true;
}

bool DLinkListIsEmpty(DLinkList &L) {
    if (L->next == nullptr) return true;
    return false;
}

bool DestroyDLinkList(DLinkList &L) {
    if (L == nullptr) return false;
    while (DeleteNextNode(L)) continue;
    free(L);
    return true;
}

bool InsertNextNode(DNode *p, int e) {
    if (p == nullptr) return false;
    DNode *q = (DNode *)malloc(sizeof(DNode));
    if (q == nullptr) return false;
    q->data = e;
    return InsertNextNode(p, q);
}

bool InsertNextNode(DNode *p, DNode *s) {
    if (p == nullptr || s == nullptr) return false;
    s->next = p->next;
    if (s->next != nullptr) s->next->prior = s;
    p->next = s;
    s->prior = p;
    return true;
}

bool InsertPriorNode(DNode *p, int e) {
    if (p == nullptr) return false;
    DNode *q = (DNode *)malloc(sizeof(DNode));
    if (q == nullptr) return false;
    q->data = e;
    if (InsertPriorNode(p, q))
        return true;
    else {
        free(q);
        return false;
    }
}

bool InsertPriorNode(DNode *p, DNode *s) {
    if (p == nullptr || s == nullptr || p->prior == nullptr) return false;
    s->prior = p->prior;
    p->prior->next = s;
    s->next = p;
    p->prior = s;
    return true;
}

bool InsertNode(DLinkList &L, int i, int e) {
    if (L == nullptr) return false;
    DNode *p = (DNode *)malloc(sizeof(DNode));
    if (p == nullptr) return false;
    p->data = e;
    if (InsertNode(L, i, p))
        return true;
    else {
        free(p);
        return false;
    }
}

bool InsertNode(DLinkList &L, int i, DNode *p) {
    if (i < 1 || L == nullptr || p == nullptr)
        return false;
    else if (i == 1)
        return InsertNextNode(L, p);
    else
        return InsertNextNode(GetElem(L, i - 1), p);
}

DNode *GetElem(DLinkList &L, int i) {
    if (L == nullptr) return nullptr;
    int j = 1;
    DNode *p = L;
    while (p->next != nullptr && j++ < i) p = p->next;
    if (p->next == nullptr) return nullptr;
    return p->next;
}

DNode *LocateElem(DLinkList &L, int e) {
    if (L == nullptr) return nullptr;
    DNode *p = L;
    while (p->next != nullptr && p->next->data != e) p = p->next;
    if (p->next == nullptr) return nullptr;
    return p->next;
}

bool DeletePriorNode(DNode *p, int &e) {
    if (p == nullptr || p->prior == nullptr || p->prior->prior == nullptr) return false;
    DNode *q = p->prior;
    return DeleteNode(q, e); 
}

bool DeletePriorNode(DNode *p) {
    int e;
    return DeletePriorNode(p, e);
}

bool DeleteNextNode(DNode *p, int &e) {
    if (p == nullptr) return false;
    return DeleteNode(p->next, e);
}

bool DeleteNextNode(DNode *p) {
    int e;
    return DeleteNextNode(p, e);
}

bool DeleteNode(DLinkList &L, int i, int &e) {
    return DeleteNode(GetElem(L, i), e);
}

bool DeleteNode(DNode *p, int &e) {
    if (p == nullptr || p->prior == nullptr) return false;
    p->prior->next = p->next;
    if (p->next != nullptr) p->next->prior = p->prior;
    e = p->data;
    free(p);
    return true;
}

bool ReverseDLinkList(DLinkList &L) {
    if (L == nullptr) return false;
    DNode *p = L->next, *q;
    L->next = nullptr;
    while (p != nullptr) {
        q = p->next;
        InsertNextNode(L, p);
        p = q;
    }
    return true;
}

int Length(DLinkList &L) {
    if (L == nullptr) return 0;
    int length = 0;
    DNode *p = L;
    while (p->next != nullptr) {
        p = p->next;
        length++;
    }
    return length;
}