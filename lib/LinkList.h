#ifndef LIB_LINKLIST_H_
#define LIB_LINKLIST_H_

typedef struct LNode {
    int data;
    struct LNode *next;
} LNode, *LinkList;

bool InitLinkList(LinkList &L);

bool LinkListIsEmpty(LinkList &L);

bool DestroyLinkList(LinkList &L);

bool InsertNode(LinkList &L, int i, int e);

bool InsertNextNode(LNode *p, int e);

bool InsertNextNode(LNode *p, LNode *s);

bool InsertPriorNode(LNode *p, int e);

bool InsertPriorNode(LNode *p, LNode *s);

bool DeleteNode(LinkList &L, int i, int &e);

bool DeleteNode(LinkList &L, int i);

bool DeleteNode(LinkList &L, LNode *p, int &e);

bool DeleteNode(LinkList &L, LNode *p);

bool DeleteNextNode(LNode *p, int &e);

bool DeleteNextNode(LNode *p);

LNode *GetElem(LinkList &L, int i);

LNode *LocateElem(LinkList &L, int e);

int Length(LinkList &L);

// LinkList ListTailInsert(LinkList &L);

// LinkList ListHeadInsert(LinkList &L);

LinkList ReverseLinkList(LinkList &L);

#endif