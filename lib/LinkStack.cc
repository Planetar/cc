#include "lib/LinkStack.h"

#include <stdio.h>
#include <stdlib.h>

bool InitLinkStack(LinkStack &s) {
    s = nullptr;
    return true;
}

bool DestroyLinkStack(LinkStack &s) {
    int e;
    while (!IsEmpty(s)) Pop(s, e);
    return true;
}

bool IsEmpty(LinkStack &s) {
    if (s == nullptr) return true;
    return false;
}

bool Push(LinkStack &s, int e) {
    LNode *p = (LNode *)malloc(sizeof(LNode));
    if (p == nullptr) return false;
    p->data = e;
    if (IsEmpty(s)) {
        s = p;
        p->next = nullptr;
        return true;
    } else {
        p->next = s;
        s = p;
        return true;
    }
}

bool Pop(LinkStack &s, int &e) {
    if (IsEmpty(s)) return false;
    LNode *p = s;
    s = s->next;
    e = p->data;
    free(p);
    return true;
}

bool GetTop(LinkStack &s, int &e) {
    if (IsEmpty(s)) return false;
    e = s->data;
    return true;
}