#include "lib/SqList.h"

#include <stdio.h>
#include <stdlib.h>

void InitSqList(SqList &L) {
    L.length = 0;
    L.maxSize = InitSize;
    L.data = (int *)malloc(sizeof(int) * InitSize);
}

void IncreaseSqListSize(SqList &L, int len) {
    int *p = L.data;
    L.data = (int *)malloc(sizeof(int) * (InitSize + len));
    for (int i = 0; i < L.length; i++) {
        L.data[i] = p[i];
    }
    L.maxSize += len;
    free(p);
}

void DestroySqList(SqList &L) {
    free(L.data);
    L.length = 0;
    L.maxSize = 0;
}

bool InsertElem(SqList &L, int i, int e) {
    if (i < 1 || i > L.length + 1)
        return false;
    else if (L.length == L.maxSize)
        return false;
    for (int j = L.length; i < j; j--) {
        L.data[j] = L.data[j - 1];
    }
    L.data[i - 1] = e;
    L.length += 1;
    return true;
}

bool DeleteElem(SqList &L, int i, int &e) {
    if (i < 1 || i > L.length) return false;
    e = L.data[i - 1];
    for (int j = i; j <= L.length; j++) {
        L.data[j - 1] = L.data[j];
    }
    L.length--;
    return true;
}

int GetElem(SqList &L, int i) {
    if (i < 1 || i > L.length) {
        printf("Get element out of index. (return -1)\n");
        return -1;
    }
    return L.data[i - 1];
}

int LocateElem(SqList &L, int e) {
    for (int j = 0; j < L.length; j++) {
        if (L.data[j] == e) return j + 1;
    }
    return 0;
}

void PrintSqList(SqList &L) {
    printf("This SqList Info:\nMaxSize: %d; Length: %d\n", L.maxSize, L.length);
    if (L.length == 0) {
        printf("No data is in the SqList.\n");
        return;
    }
    for (int i = 0; i < L.length; i++) {
        printf("%d: %d;", i + 1, L.data[i]);
    }
    printf("\n");
}