#ifndef LIB_SQSTACK_H_
#define LIB_SQSTACK_H_
#define maxSize 10

typedef struct SqStack {
    int top;
    int size;
    int *data;
} SqStack;

bool InitSqStack(SqStack &s);

bool GetTop(SqStack &s, int &e);

bool Push(SqStack &s, int e);

bool Pop(SqStack &s, int &e);

bool IsEmpty(SqStack &s);

bool IsFull(SqStack &s);

bool DestroySqStack(SqStack &s);

#endif