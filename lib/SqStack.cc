#include "lib/SqStack.h"

#include <stdio.h>
#include <stdlib.h>

bool InitSqStack(SqStack &s) {
    s.data = (int *)malloc(sizeof(int) * maxSize);
    if (s.data == nullptr) return false;
    s.top = -1;
    s.size = maxSize;
    return true;
}

bool GetTop(SqStack &s, int &e) {
    if (IsEmpty(s)) return false;
    e = s.data[s.top];
    return true;
}

bool Push(SqStack &s, int e) {
    if (IsFull(s)) return false;
    s.data[++s.top] = e;
    return true;
}

bool Pop(SqStack &s, int &e) {
    if (IsEmpty(s)) return false;
    e = s.data[s.top--];
    return true;
}

bool IsEmpty(SqStack &s) {
    if (s.top == -1)
        return true;
    else
        return false;
}

bool IsFull(SqStack &s) {
    if (s.top == s.size - 1)
        return true;
    else
        return false;
}

bool DestroySqStack(SqStack &s) {
    if (s.data == nullptr) return false;
    free(s.data);
    s.top = -1;
    s.size = 0;
    return true;
}