#ifndef LIB_LINKSTACK_H_
#define LIB_LINKSTACK_H_

typedef struct LNode
{
    int data;
    LNode *next;
} LNode, *LinkStack;

bool InitLinkStack(LinkStack &s);

bool DestroyLinkStack(LinkStack &s);

bool IsEmpty(LinkStack &s);

bool Push(LinkStack &s, int e);

bool Pop(LinkStack &s, int &e);

bool GetTop(LinkStack &s, int &e);

#endif