#include "lib/LinkList.h"

#include <stdio.h>
#include <stdlib.h>

bool InitLinkList(LinkList &L) {
    L = (LNode *)malloc(sizeof(LNode));
    if (L == nullptr) return false;
    L->data = 0;
    L->next = nullptr;
    return true;
}

bool LinkListIsEmpty(LinkList &L) {
    if (L->next == nullptr) return true;
    return false;
}

bool DestroyLinkList(LinkList &L) {
    if (L == nullptr) return false;
    while (DeleteNextNode(L)) continue;
    free(L);
    return true;
}

LNode *GetElem(LinkList &L, int i) {
    if (L == nullptr) return nullptr;
    int j = 1;
    LNode *p = L;
    while (p->next != nullptr && j++ < i) p = p->next;
    if (p->next == nullptr) return nullptr;
    return p->next;
}

LNode *LocateElem(LinkList &L, int e) {
    if (L == nullptr) return nullptr;
    LNode *p = L;
    while (p->next != nullptr && p->next->data != e) p = p->next;
    if (p->next == nullptr) return nullptr;
    return p->next;
}

bool DeleteNextNode(LNode *p, int &e) {
    if (p == nullptr || p->next == nullptr) return false;
    LNode *tmp = p->next;
    p->next = p->next->next;
    e = tmp->data;
    free(tmp);
    return true;
}

bool DeleteNextNode(LNode *p) {
    int e;
    return DeleteNextNode(p, e);
}

bool DeleteNode(LinkList &L, LNode *p, int &e) {
    if (p == nullptr || L == nullptr || L == p)
        return false;
    else if (p->next == nullptr) {
        LNode *q = L;
        while (q->next->next != nullptr) q = q->next;
        DeleteNextNode(q, e);
        return true;
    } else {
        e = p->data;
        LNode *q = p->next;
        p->data = q->data;
        p->next = q->next;
        free(q);
        return true;
    }
}

bool DeleteNode(LinkList &L, LNode *p) {
    int e;
    return DeleteNode(L, p, e);
}

bool DeleteNode(LinkList &L, int i, int &e) {
    if (i < 1)
        return false;
    else if (i == 1)
        return DeleteNextNode(L, e);
    LNode *p = GetElem(L, i - 1);
    if (p == nullptr || p->next == nullptr) return false;
    return DeleteNextNode(p, e);
}

bool DeleteNode(LinkList &L, int i) {
    int e;
    return DeleteNode(L, i, e);
}

bool InsertNextNode(LNode *p, int e) {
    LNode *q = (LNode *)malloc(sizeof(LNode));
    if (q == nullptr || p == nullptr) return false;
    q->next = p->next;
    q->data = e;
    p->next = q;
    return true;
};

bool InsertNextNode(LNode *p, LNode *s) {
    if (p == nullptr || s == nullptr) return false;
    s->next = p->next;
    p->next = s;
    return true;
};

bool InsertPriorNode(LNode *p, int e) {
    if (p != nullptr && InsertNextNode(p, e)) {
        int tmp = p->data;
        p->data = p->next->data;
        p->next->data = tmp;
        return true;
    } else
        return false;
};

bool InsertPriorNode(LNode *p, LNode *s) {
    if (p == nullptr || s == nullptr) return false;
    int tmp = s->data;
    s->data = p->data;
    p->data = tmp;
    return InsertNextNode(p, s);
};

bool InsertNode(LinkList &L, int i, int e) {
    if (L == nullptr || i < 1) return false;
    if (i == 1) return InsertNextNode(L, e);
    LNode *p = GetElem(L, i - 1);
    if (p == nullptr) return false;
    return InsertNextNode(p, e);
}

int Length(LinkList &L) {
    if (L == nullptr) return 0;
    int length = 0;
    LNode *p = L;
    while (p->next != nullptr) {
        p = p->next;
        length++;
    }
    return length;
}

// LinkList ListTailInsert(LinkList &L) {}

// LinkList ListHeadInsert(LinkList &L) {}

LinkList ReverseLinkList(LinkList &L) {
    if (L == nullptr || L->next == nullptr) return L;
    LNode *p = L->next, *q;
    L->next = nullptr;
    while (p != nullptr) {
        q = p;
        p = p->next;
        InsertNextNode(L, q);
    }
    return L;
}