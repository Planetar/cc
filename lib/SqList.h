#ifndef LIB_SQLIST_H_
#define LIB_SQLIST_H_
#define InitSize 10

typedef struct {
    int *data;
    int maxSize;
    int length;
} SqList;

void InitSqList(SqList &L);

void IncreaseSqListSize(SqList &L, int len);

void DestroySqList(SqList &L);

bool InsertElem(SqList &L, int i, int e);

bool DeleteElem(SqList &L, int i, int &e);

int GetElem(SqList &L, int i);

int LocateElem(SqList &L, int e);

void PrintSqList(SqList &L);

#endif