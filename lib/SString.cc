#include "lib/SString.h"

#include <stdio.h>

bool InitSString(SString &s) {
    s.length = 0;
    return true;
}

bool ClearSString(SString &s) {
    s.length = 0;
    return true;
}

bool DestroySString(SString &s) {
    s.length = 0;
    return true;
}

bool StrAssign(SString &target, char src[]) {
    int i = 0;
    while (src[i] != '\0' && i < MAXLEN) {
        target.ch[i] = src[i];
        i++;
    }
    if (i == MAXLEN) return false;
    target.length = i;
    return true;
}

bool StrCopy(SString &target, SString src) {
    for (int i = 0; i < src.length; i++) target.ch[i] = src.ch[i];
    target.length = src.length;
    return true;
}

bool StrPrint(SString s) {
    for (int i = 0; i < s.length; i++) printf("%c", s.ch[i]);
    printf("\n");
    return true;
}

bool StrEmpty(SString s) {
    if (s.length == 0) return true;
    return false;
}

int StrLength(SString s) { return s.length; }

bool Concat(SString &target, SString src1, SString src2) {
    if (src1.length + src2.length > MAXLEN) return false;
    int i = 0;
    for (; i < src1.length; i++) target.ch[i] = src1.ch[i];
    for (; i < src1.length + src2.length; i++)
        target.ch[i] = src2.ch[i - src1.length];
    target.length = src1.length + src2.length;
    return true;
}

bool SubString(SString &sub, SString src, int pos, int len) {
    if (pos + len - 1 > src.length) return false;
    sub.length = len;
    for (int i = 0; i < len; i++) sub.ch[i] = src.ch[pos + i - 1];
    return true;
}

int Index(SString src, SString sub) {
    SString this_sub;
    for (int i = 0; i < src.length - sub.length + 1; i++) {
        SubString(this_sub, src, i + 1, sub.length);
        if (StrCompare(sub, this_sub) == 0) return i + 1;
    }
    return 0;
}

int StrCompare(SString s1, SString s2) {
    int i = 0;
    while (i < s1.length && i < s2.length) {
        if (s1.ch[i] != s2.ch[i]) return s1.ch[i] - s2.ch[i];
        i++;
    }
    return s1.length - s2.length;
}