#ifndef LIB_SSTRING_H_
#define LIB_SSTRING_H_
#define MAXLEN 255

typedef struct SString {
    int length;
    char ch[MAXLEN];
} SString;

bool InitSString(SString &s);

bool ClearSString(SString &s);

bool DestroySString(SString &s);

bool StrAssign(SString &target, char src[]);

bool StrCopy(SString &target, SString src);

bool StrPrint(SString s);

bool StrEmpty(SString s);

int StrLength(SString s);

bool Concat(SString &target, SString src1, SString src2);

bool SubString(SString &sub, SString src, int pos, int len);

int Index(SString src, SString sub);

int StrCompare(SString S, SString T);

#endif