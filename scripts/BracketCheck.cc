#include "scripts/BracketCheck.h"

#include <stdio.h>
#include <stdlib.h>

#include "lib/LinkStack.h"

bool BracketCheck(char str[], int length) {
    LinkStack s;
    InitLinkStack(s);
    for (int i = 0; i < length; i++) {
        if (str[i] == '{' || str[i] == '[' || str[i] == '(')
            Push(s, str[i]);
        else if (str[i] == '}' || str[i] == ']' || str[i] == ')') {
            int e = 0;
            GetTop(s, e);
            if (str[i] == '}' && e != '{') {
                printf("Expected '{' in %d\n", i);
                DestroyLinkStack(s);
                return false;
            } else if (str[i] == ']' && e != '[') {
                printf("Expected '[' in %d\n", i);
                DestroyLinkStack(s);
                return false;
            } else if (str[i] == ')' && e != '(') {
                printf("Expected '(' in %d\n", i);
                DestroyLinkStack(s);
                return false;
            } else if (e == 0) {
                printf("No left bracket match %c\n", str[i]);
                return false;
            } else
                Pop(s, e);
        }
    }
    if (!IsEmpty(s)) {
        printf("Expected right bracket.\n");
        return false;
    }
    DestroyLinkStack(s);
    return true;
}