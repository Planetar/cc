# cc

#### Introduction
This repo is about cc.

#### Usage

1.  Clone this repo
2.  Install [bazel](https://bazel.build/)
3.  Use `make` to build the binary files
4.  Use `make run` to run the main function
5.  Use `make clean` to clean up the "bazel-*" files
6.  Use `make test` to run all tests

#### How to contribute

1.  Fork this repo
2.  New Feat_xxx branch
3.  Commit your code
4.  New Pull Request