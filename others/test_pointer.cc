#include <iostream>
using namespace std;

int main() {
    int length;
    cout << "Please input the length of the list[int]: " << endl;
    cin >> length;
    cout << "The length is: " << length << endl;

    cout << "Now, please input the list number:" << endl;
    int *pList = new int[length];
    for (int i = 0; i < length; i++) {
        int tmp_value;
        cin >> tmp_value;
        *(pList + i) = tmp_value;
    }
    for (int i = 0; i < length; i++) {
        cout << "The " << i << " number: " << pList[i] << endl;
    }

    delete pList;
    return 0;
}
