#include <ctime>
#include <iostream>
using namespace std;

int main() {
    float sec;
    cout << "Please input the time you want to stop: " << endl;
    cin >> sec;

    clock_t delay = sec * CLOCKS_PER_SEC;
    time_t start_time;
    start_time = time(NULL);
    cout << "Now is: " << asctime(localtime(&start_time))
         << "And you want to stop " << sec << "(s)" << endl;
    clock_t start = clock();
    while (clock() - start < delay)
        ;
    time_t end_time;
    end_time = time(NULL);
    cout << "done" << endl
         << "Now is: " << asctime(localtime(&end_time)) << endl;
    return 0;
}
