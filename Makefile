.PHONY: all build rebuild clean run
all: clean build

build:
	@ echo "Starting building ..."
	@ bazel build //main:main

rebuild: clean
	@ echo "Start rebuilding ..."
	@ bazel build //main:main

clean:
	@ echo "Cleaning up ..."
	rm -rf ./bazel-*

run:
	@ echo "Running main ..."
	@ ./bazel-bin/main/main

test:
	@ echo "Running tests ..."
	@ bazel test ... --test_output all